# README #

Wouldn't it be nice if messages in your app had oracle-like message codes but you did not have to bother maintaining message tables. That's work the computer should be doing, right?

This project allows you to annotate code as 

    @Message("APP-0011")
    private static final String msg = "Exception occurred with {} lookup.";

Compile time annotation processors then 
1. integrate with slf4j to prefix the error code in the log using a %msgId format key and 
2. produce an error table like the following, that you can import into a wiki


|Message ID|Message Text|Wiki Link|
|APP-0000|foo|<wiki link to error desc>|
|APP-0001|Hello, world -- instance level|<wiki link 2>|
|APP-0002|whatchew doing -- instance level|<wiki link 3>|